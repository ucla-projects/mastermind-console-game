//
//  Score.cpp
//

#include "Score.h"
#include <stdexcept>

namespace cs31
{

// setup a default score - all WRONG ANSWER's
Score::Score()
{
    for( int i = 0; i < REQUIREDLENGTH; i++ )
    {
        array[ i ] = WRONG;
    }
}

//*************** TO DO *****************
// TODO comparing the played move to the answer move,
//      build the ANSWER array of this Score
Score::Score( Move move, Move answer )
{

    for (int i = 0; i < REQUIREDLENGTH; i++)
    {
        array[i] = WRONG;
    }
    for (int i = 0; i < REQUIREDLENGTH; i++)
    {
        Piece a = move.getPiece(i);
        Piece b = answer.getPiece(i);
        if (a.getColor() == b.getColor())
        {
            array[i] = RIGHT;
        }
    }
    for (int i = 0; i < REQUIREDLENGTH; i++)
    {
        if (array[i] == WRONG)
        {
            for (int j = 0; j < REQUIREDLENGTH; j++)
            {
                if (j != i && array[j] != RIGHT)
                {
                    Piece a = move.getPiece(i);
                    Piece b = answer.getPiece(j);
                    if (a.getColor() == b.getColor())
                    {
                        array[i] = MAYBE;
                    }
                }
            }
        }
    }
}

// trivial getter but throw logic_error if the index is out of range
ANSWER Score::getAnswer( int i )
{
    if (i >= 0 && i < REQUIREDLENGTH)
        return( array[ i ] );
    else
        throw std::logic_error( "invalid i value" );
}

// stringify this Score
std::string Score::to_string() const
{
    std::string s = "";
    int mCount = 0;
    int rCount = 0;
    for (int i = 0; i < REQUIREDLENGTH; i++)
    {
        switch( array[i] )
        {
            case WRONG:
                // wrongs should not be printed at all
                s += "_";
                break;
            case RIGHT:
                rCount++;
                s += "R";
                break;
            case MAYBE:
                mCount++;
                s += "M";
                break;
        }
    }
    return( s );
}

// TODO is the ANSWER array all RIGHT ANSWER's?
bool Score::isExactMatch() const
{
    bool ret = false;
    int count = 0;
    for (int i = 0; i < REQUIREDLENGTH; i++)
    {
        ANSWER temp = array[i];
        if (temp == RIGHT)
        {
            count++;
        }
    }
    if (count == 4)
    {
        ret = true;
    }
    return( ret );
}

}

